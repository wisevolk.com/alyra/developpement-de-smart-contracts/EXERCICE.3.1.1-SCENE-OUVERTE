pragma solidity ^0.6.2;
contract SceneOuverte {

    string[12] private passageArtistes;
    uint8 private creneauxLibre;
    uint8 tour;

    constructor() public {
        creneauxLibre = 12;
        tour = 0;
    }

    function sInscrire(string memory _artiste) public {
        if(creneauxLibre > 0){
            passageArtistes[passageArtistes.length] = _artiste;
            creneauxLibre--;
        }
    }

    function passerArtisteSuivant() public {
        if(tour < 12)
            tour++;
    }

    function artisteEncours() public view returns (string memory){
        if(tour < 12){
            return passageArtistes[tour];
        } else {
            return "FIN";
        }
    }
}
